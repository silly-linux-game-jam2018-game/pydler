README
======

Pydler requires python3 (3.3 and up).  
Audio depends on pySDL2, SDL2 and SDL_mixer.


Pydler is text adventure engine under GPLv3 and it's currently loading content for the game ENDUSER.

If you are willing to use your own terminal, launch it and execute run.sh or let run_with_xterm.sh do it for you.

You can start the new, slightly shinier terminal user interface by providing --tui as a launch parameter.


Made by Tuubi, Samsai and Tumocs  
Foundation for the TUI, based on picotui, graciously contributed by Ysblokje
