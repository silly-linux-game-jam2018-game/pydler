#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#

### Game locations
# Note that naming policy is different from objects. The basic
# requirement is that the name works after "You are in ".

main_room = Location("the main room of your apartment",
    "This is what passes for the bedroom, the living room and the kitchen in "
    "your minuscule apartment. You've seen larger sardine cans.",
    "The verb " + green("use") + " allows you to interact with the game world. "
    "If you want to use an item with another, " + green("on") + " and " + green("with") + " work as separators.\n\n"
    "For example, "+ green("use eyes") + " or " + green("use hand on bathroom door") + ".\n\n"
    "Access your inventory of items (and... things) with " + green("use eyes on self") + " or " + green("use eyes on inventory") + ".\n\n"
    "You can hit " + blue("<<completion_key>>") + " to complete a word if you're in a hurry.")

bathroom = Location("your bathroom",
    "Your tiny (and somewhat dirty) bathroom. Adequate for relieving and "
    "bathing purposes, not for much else.",
    "This cramped space facilitaties a variety of pastimes. Just take a seat "
    "and stink, think, crap or fa... Fiddle.")
hallway = Location("the hallway outside your apartment",
    "Narrow and badly lit, the white-painted, long hallway is lined with doors "
    "to tiny apartments, one of them yours.\n\n"
    "At least it's relatively clean.",
    "If outside is hell, this must be purgatory. Standing here you simply "
    "can't ignore the fact that you are always surrounded by strangers, and "
    "the safety of your apartment is just an illusion.")

### Objects
# Use lowercase for the names and make them unique and preferably easy
# to type. If a thing - like a door - can be seen from two locations,
# it needs to be two separate objects.

# main_room
fridge = GameObject("fridge",
    "Refrigerated food container unit. Belonging to you.",
    "You think the corner grocery store should still be open. "
    "You're not sure if it has ever " + underline("not") + " been open. "
    "Must be one of those 24/7 places.")
fridge.add_use("default", "Yes, you know you're not a cool person by any "
    "definition of the word. No need to rub it in.")

hand.add_use("fridge",
    "Wishing for a miracle, you peek inside. What a sorry sight: The "
    "desiccated remains of a cucumber and two empty bottles of ketchup. "
    "Your stomach lets out a sad little groan.")

table = GameObject("table",
    "The rickety table is sitting in the corner of the room. Its only "
    "purpose is to hold your dirty old PC, and so far it has done the job.")
table.add_use("default",
    "The table is fully occupied by your PC. No room for you.")

hand.add_use("table",
    "The table does its signature wobble when you nudge it. The "
    "accompanying creak is new. It was never very stable, but at least "
    "it had the decency to suffer your abuse in silence.")

keys = GameObject("keys",
    "A small bundle of keys. It holds the one necessary key to get back in "
    "and a bunch of other keys you've just never returned or gotten rid of.",
    "These might help you get back in.")
keys.add_use("default", "No, none of these keys is the key to your heart.")
keys.load_audio("keys.ogg")
keys.add_alias("key")

main_room.add_object(keys)

def pick_keys():
    cont.gs.add_to_inventory(keys)
    keys.play_audio()

hand.add_use("keys", pick_keys)

def hand_on_computer_action():
    if cont.gs.get_flag("liberated"):
        cont.gs.say("You pat the computer gently. Good computer.")
    else:
        cont.gs.say("You smack the side of the monitor of your pitiful PC. Bad computer.");

hand.add_use("pc", hand_on_computer_action)

computer = GameObject("PC",
    "It's a dirty PC quickly closing in on obsolescence. Still plays "
    "some older games and browses the web, on a good day.\n\n"
    "It runs a motley collection of malware, including whatever version "
    "of Windows came preinstalled all those years ago.",
    "You've been meaning to switch to Linux for the last... twelve years "
    "or so? The live-CD you burned should still be around here somewhere."
    "\n\nYou can't think of a good excuse for never making the effort.")
computer.add_alias("computer")
computer.add_use("default",
    "\"BEEP BOOP.\"\n\nThe computer does not answer your friendly greeting.")

wallet = GameObject("wallet",
    "A red and blue nylon children's wallet with a Minnie Mouse pattern "
    "on the back.\n\n"
    "Way too childish for an adult, but you don't care. It's the perfect "
    "size for your single debit card and a few bits of loose change. Not "
    "to forget your lucky condom, still pristine in its vintage foil cover.",
    "Thinking about your wallet fills you with determ... despondence.")
wallet.add_use("default",
    "You stroke Minnie affectionately with your thumb. You've always had a "
    "soft spot for the squeaky little lady.")
hand.add_use("wallet", lambda : cont.gs.add_to_inventory(wallet))

couch = GameObject("couch",
    "This ancient, black, faux-leather monstrosity covers close to a "
    "third of your meagre floor space, almost blocking the front door.\n\n"
    "A red quilt blanket lies half on the cushions, half on the floor. "
    "In one end the face of a well-known bearded revolutionary stares "
    "at you from the printed cover of a greasy throw pillow.\n\n"
    "Yes, this is where you sleep. How sad is that?",
    "You wonder why the pillow is at the end where you keep your feet. "
    "You don't remember moving it there. For some reason this really bugs you.")
couch.add_alias("sofa")

def end_game_home():
    cont.gs.say("You lie down on the couch and go to sleep.")
    cont.gs.pause(2)
    sleep()
    cont.gs.pause(2)
    cont.gs.say("You sleep through the night. Game over.")
    cont.gs.quit("Press any key to skedaddle.")

couch.add_use("default", end_game_home)

main_room.add_object(couch)

def move_pillow():
    cont.gs.say("You quickly fold the blanket and set it down on the couch. "
        "As you pick up the pillow to shift it to its proper place, you "
        "notice your wallet sticking out from the crack at the base of the "
        "arm rest. Huh. Guess your effort \"paid off\".")
    couch.desc = ("The couch is still ugly, but at least your \"bed\" is "
        "now ready for you to plunk down in.")
    couch.hint = "Much better. Your chum Guevara is back where he belongs."
    main_room.add_object(wallet)
    hand.delete_use('couch')
    return False
hand.add_use("couch", move_pillow)

door_to_bathroom = GameObject("bathroom door",
    "The slightly warped plywood door sports a sticker with a cheerful "
    "little cherub letting loose a golden stream of happily splashing "
    "urine.\n\n"
    "You guess the previous tenant thought some angel piss might brighten "
    "the otherwise bleak apartment.",
    "You decide to flex your telekinetic powers for a bit. Nnnngh!\n\n"
    "Nope, the door refuses to aknowledge your superior mind and stays closed.")
door_to_bathroom.add_use('default', lambda : cont.gs.move_to(bathroom))
hand.add_use("bathroom door", lambda : cont.gs.move_to(bathroom))
door_to_bathroom.load_audio("door.ogg")
keys.add_use("bathroom door", "There's no lock on the door.")

bathroom.on_enter = lambda : door_to_bathroom.play_audio()
bathroom.on_leave = lambda : door_to_bathroom.play_audio()

linuxcd = GameObject("GNU+Linux",
    "A CD-ROM. The insert in the cracked jewel case sports a crude pencil "
    "drawing of a penguin riding a feisty looking gnu.",
    "You could use it to liberate your PC.")
linuxcd.add_use("default",
    "You stand transfixed by the colourful reflection on the underside of "
    "the CD for a while, ruminating on all that delicious freedom encoded "
    "in the barely discernible track seared into the pristine surface.")

linuxcd.add_use("legs",
    "They say Linux runs faster but I doubt the effect would transfer over "
    "to you.")

def install_gnu_plus_linux():
    cont.gs.say("You pop the installation CD into the drive. Your trusty "
        "machine whirrs and pops and crackles. You follow the simple "
        "instructions that appear on screen all the way to the end.\n\n"
        "You are freed from absolutely proprietary software and your life is "
        "finally complete.")
    cont.gs.increment_score(1000)
    computer.desc = ("Now your computer runs on freedom. It's blazingly fast "
        "and you bathe in the glory of Stallman.\n\nYou cry tears of joy.")
    computer.hint = ("Your computer is just the way it's supposed to be now.")
    cont.gs.set_flag("liberated", True)
    cont.gs.inventory.remove(linuxcd)

linuxcd.add_use("pc", install_gnu_plus_linux)

closet = GameObject("closet",
    "A wooden, rectangular receptacle that is commonly used to hold "
    "clothes. You store most of them on your floor. The closet might still "
    "hold a few clean items of clothing within.",
    "You might find bits to cover your meaty bits with in there.")

main_room.add_object(closet)

def rummage_closet():
    if not cont.gs.get_flag("clothed"):
        cont.gs.say("You drag out and quickly slip into a faded but clean "
            "pair of denims and a mustard coloured T-shirt.\n\n"
            "You suppose the famous queer eye might not approve of the "
            "ensemble, but at least your anatomy is now shielded from "
            "unwanted scrutiny.")
        cont.gs.set_flag("clothed")
        cont.gs.set_flag("rummaged", 1)
        cont.gs.increment_score(1)
    else:
        rummaged = cont.gs.get_flag("rummaged")
        if rummaged < 2:
            cont.gs.set_flag("rummaged", rummaged + 1)
            cont.gs.say("You spend a while rummaging through the pile of "
                "clothes and miscellanea on the closet floor. "
                "You give up after a few minutes, but feel like you should "
                "go for a proper treasure raid some time soon.")
        else:
            cont.gs.say("You decide to give it another go, and this time "
                "you strike gold! Not literally, but you did find the "
                "GNU+Linux live-CD you burned years ago to try out.")
            main_room.add_object(linuxcd)
            cont.gs.add_to_inventory(linuxcd)
            hand.delete_use("closet")
            hand.add_use("closet", "You've rummaged enough for one day.")
hand.add_use("closet", rummage_closet)

closet.add_use("default", "You fondle the closet for a bit and wonder what use "
        "you might make of it. Oh, maybe you could " + green("use") + " your " +
        green("hand") + " with it to fetch some clothes to wear!")

door_to_hallway = GameObject("hallway door",
    "A sturdy door with a mail slot you've taped over. You haven't seen "
    "bills in ages thanks to that brilliant stroke of genius, and at this "
    "point you don't even want to know what you've missed.",
    "You think you could use this to exit your apartment. The thought "
    "makes you uneasy.")
door_to_hallway.add_alias("exit")
door_to_hallway.add_use("default", lambda : cont.gs.move_to(hallway))
hand.add_use("hallway door", lambda : cont.gs.move_to(hallway))
keys.add_use("hallway door",
    "You're on the wrong side of the door, you dolt. You don't need a key to "
    "get out.")

door_to_hallway.load_audio("outdoor.ogg")

main_room.add_object(fridge)
main_room.add_object(table)
main_room.add_object(computer)
main_room.add_object(door_to_bathroom)
main_room.add_object(door_to_hallway)

# bathroom
bathroom_exit = GameObject("door to main room",
    "This side of the door bears an impressive, multilayered collection of "
    "stains and water damage. It's a wonder it hasn't outright rotted "
    "through.\n\nYou used to hang your bath towel on a nail sticking out "
    "of it, but that probably made the problem worse.",
    "As long as the world geometry stays Euclidean that door should lead "
    "to where you came from: the main room of your apartment.")
bathroom_exit.add_alias("door")
bathroom_exit.add_use("default", lambda : cont.gs.move_to(main_room))
hand.add_use("door to main room", lambda : cont.gs.move_to(main_room))
keys.add_use("door to main room", "There's no lock. Just open it.")

toilet = GameObject("toilet",
    "The porcelain throne. The meditation pedestal. The rumble chair.\n\n"
    "Takes your foulest shit and never complains.",
    "You like to think on the toilet, but the toilet itself doesn't bear "
    "thinking about.")
hand.add_use("toilet",
    "Fwoosh! The sound of a flushing toilet must be one of the most "
    "satisfying things in the universe.")
toilet.add_use("default",
    "You sit on the throne and spread your butt-cheeks wide apart. You "
    "start squeezing hard, expecting a massive python to eject through your "
    "sphincter but to your disappointment the only thing that escapes is an "
    "earth shattering fart.\n\n"
    "You give your anus a quick wipe just in case and stand up, let down by "
    "your bowel.")

shower = GameObject("shower",
    "In one end of the narrow bathroom a vinyl shower curtain hides a "
    "rudimentary shower set. There's something wrong with the thermostat: "
    "You never know if it's going to scald you or freeze your bits off.\n\n"
    "Could be worse you suppose. Not like you use it often enough to care.",
    "You don't really understand why washing yourself is such a daunting "
    "task that you keep putting it off. Maybe you'd just rather not give "
    "yourself the time to think about all that is wrong with you and your "
    "so-called life.")
shower.load_audio("shower.ogg")

def wash_thyself():
    if cont.gs.get_flag("showered"):
        cont.gs.say("You didn't enjoy the experience that much. "
            "You're clean enough already.")
    else:
        shower.play_audio()
        cont.gs.say("You strip and slip under the shower, careful to "
            "move the curtain as little as possible. The flimsy railing it "
            "hangs from keeps dislodging and trying to impale you.\n\n"
            "The water pressure starts off really low, but the pitiful "
            "flow is less of a problem than the way the temperature keeps "
            "fluctuating between sub-zero and hellfire. You grit your teeth "
            "and endure the torture for the three minutes it takes you to "
            "scrub the worst of the stink and the filth off your scrawny "
            "corpus.\n\nJust before you're done, the fancy (but cheap) "
            "shower head decides to jam into \"massage\" mode, surprising "
            "you with a painful spout of frigid water. Good thing you saved "
            "washing the most sensitive bits for last...")
        if not cont.gs.get_flag("clothed"):
            cont.gs.say("You wipe yourself relatively dry with the musty "
                "towel hanging next to the mirror, then slip into your "
                "tank top and boxers.")
        else:
            cont.gs.say("\nYou wipe yourself relatively dry with the musty "
                "towel hanging next to the mirror, then get dressed again.")
        cont.gs.set_flag("showered")
        cont.gs.set_flag("foggy mirror")
        cont.gs.increment_score(1)
        hand.add_use("mirror", clean_mirror)
        shower.hint = shower.hint + \
            "\n\nOh yeah, now you remember. Even the shower hates you."
shower.add_use("default", wash_thyself)

def clean_mirror():
    cont.gs.say("You wipe at the foggy mirror with your arm until you can "
        "just barely see your reflection.")
    cont.gs.unset_flag("foggy mirror")
    hand.delete_use("mirror")

mirror = GameObject("mirror",
    "The object that reflects all the things. Or it used to when it wasn't "
    "quite as dirty. You should still be able to see yourself in it if you "
    "really wanted to.\n\nMaybe you should just wait until it's completely "
    "opaque. You could look at it. But maybe you shouldn't.",
    "You think normal people occasionally check their own appearance in a "
    "mirror. Perhaps to remind themselves what they look like, in case they "
    "need to recognize a doppelganger? No, that doesn't make sense.\n\n"
    "You've never been very comfortable with things others would consider "
    "normal. You suppose that makes you abnormal.")
def check_mirror():
    if not cont.gs.get_flag("mirror seen"):
        cont.gs.say(mirror.desc)
        cont.gs.set_flag("mirror seen")
        return False
    if cont.gs.get_flag("foggy mirror"):
        cont.gs.say("You can't see anything in the mirror due to the water "
            "condensation fogging up its surface. In your experience it "
            "will not clear up anytime soon on its own. The humidity will "
            "linger for hours.")
        return False
    if cont.gs.get_flag("showered"):
        if cont.gs.get_flag("shaved"):
            cont.gs.say("The merciless reflector shows mercy! The apparition "
                "in the mirror looks almost presentable.\n\n"
                "You're no beauty, but you're starting to look... "
                "inconspicuous. What a lovely word.")
            if not cont.gs.get_flag("clothed"):
                cont.gs.say("\nAlthough you're not quite as enthusiastic about "
                    "the boxers and the tank top.")
        else:
            cont.gs.say("Clean if not exactly neat. You decide to consider "
                "it a win and awkwardly avert your gaze.")
    elif cont.gs.get_flag("shaved"):
        cont.gs.say("The mirror shows a dirty creature. Its pale skin is "
            "hairless in the spots where you scraped away at the filth with "
            "your razor blade. Not worse than before, but not much of an "
            "improvement either.")
    else:
        cont.gs.say("You instinctively flinch away from the filthy thing "
            "peeking at you from the dirty glass surface. You'd like to "
            "pretend it's a cruel joke involving real-time image "
            "manipulation, but you can smell the truth. Literally.")

eyes.add_use("mirror", check_mirror)

razor = GameObject("razor",
    "A straight edge razor you acquired back when you actually shaved parts of "
    "your body.",
    "It could be used to cut things. Even body hair.")
def handle_razor():
    if razor not in cont.gs.inventory:
        cont.gs.add_to_inventory(razor)
    else:
        cont.gs.say("You slide your fingers along the spine of the "
            "blade. For just a moment something unsettlingly familiar takes "
            "form behind your eyes and your chest feels unbearably tight.\n\n"
            "You force a deep breath and pretend it was nothing. You don't "
            "like what the razor does to your mind.\n\n"
            "Blaming the blade might be unfair, but the alternative scares "
            "you shitless.")
hand.add_use("razor", handle_razor)
def razorize():
    if cont.gs.location == bathroom and not cont.gs.get_flag("shaved"):
        if cont.gs.get_flag("showered"):
            cont.gs.say("You take a moment to very carefully apply the "
                "blade to appropriate areas of your skin. The edge is in "
                "surprisingly good condition after all this time in a humid "
                "bathroom and doesn't do lasting damage.")
        else:
            cont.gs.say("You carefully slide the blade through "
                "appropriate areas of your filth and sweat-encrusted skin, "
                "giving it an odd, patchwork look. The edge is in surprisingly "
                "good condition after all this time in a humid bathroom and "
                "doesn't do lasting damage.")
        cont.gs.set_flag("shaved")
    else:
        cont.gs.say("You squeeze the handle of the razor, but you "
            "don't know what you're supposed to do with it.")
razor.add_use("default", razorize)

razor.add_use("hand",
    "Ouch! A drop of blood oozes from the tiny cut you made on the side of "
    "your index finger. This might not be a productive use of your time, "
    "or the razor.")

razor.add_use("legs", "Shave your legs? What for?")
razor.add_use("couch", "You don't want to mutilate your bed. It's "
    "uncomfortable enough as it is.")

razor.add_use("wallet",
    "You have plenty of reasons to feel inadequate, but please don't take it "
    "out on poor Minnie.")

bathroom.add_object(bathroom_exit)
bathroom.add_object(shower)
bathroom.add_object(toilet)
bathroom.add_object(mirror)
bathroom.add_object(razor)

# hallway

def friendly_judgement():
    door_to_hallway.play_audio()
    if not cont.gs.get_flag("judged"):
        if cont.gs.get_flag("clothed") and cont.gs.get_flag("showered") and \
                cont.gs.get_flag("shaved"):
            cont.gs.say("Your neighbour is visibly surprised by your appearance. "
                    "You can see a faint smile on his face. He nods at you.")
        elif cont.gs.get_flag("clothed"):
            if cont.gs.get_flag("showered"):
                cont.gs.say("Neighbour looks at you and nods.")
            if not cont.gs.get_flag("showered"):
                cont.gs.say("Your neighbour greets you. Slowly your odor creeps "
                        "up to his nose. He is slightly disgusted.")
        elif not cont.gs.get_flag("clothed"):
            if not cont.gs.get_flag("showered"):
                cont.gs.say("Your neighbour is visibly disgusted, he shakes his "
                        "head in disappointment. He seems to be tired of looking "
                        "at your trainwreck of a life.")
            else:
                cont.gs.say("Your neighbour looks at your unclothed body and "
                        "shakes his head in disappointment")
        cont.gs.set_flag("judged")


hallway.on_enter = friendly_judgement
hallway.on_leave = lambda : door_to_hallway.play_audio()

neighbour = GameObject("neighbour",
    "The nosy, judgmental neighbour. There must be a law that states every "
    "apartment block must have at least one.",
    "Do not harm it.")
neighbour.add_use("default", "You fondle the neighbour for a bit. "
    "It's hard to tell if the neighbour approves.")
wallet.add_use("neighbour", "The judgment comes free of charge.")

def cannot_kill_em_all():
    cont.gs.say("You try to swing the razor at the neighbour but they dodge "
        "and respond with a quick punch to your face. Ouch.\n\n"
        "After having shown you your place in the pecking order, they leave.")
    hallway.remove_object(neighbour)
    cont.gs.decrement_score(1)

razor.add_use("neighbour", cannot_kill_em_all)

#TODO: implement neighbour responses and check on first entry to hallway
door_to_street = GameObject("outer door",
    "A massive metal frame holding pieces of reinforced glass. You hope "
    "you possess the necessary strength today to push it open.",
    "You could go outside. Is your hunger great enough? What's a few weeks "
    "of fasting to avoid human interaction...")
door_to_street.add_alias("exit")
door_to_street.add_use("default",
    lambda : cont.gs.move_to(apartment_street))
hand.add_use("outer door", lambda : cont.gs.move_to(apartment_street))
keys.add_use("outer door",
    "Instead of a keyhole, there's a little metal lever on this side. Even if "
    "the door was locked, you wouldn't need a key to get out.")

apartment_door = GameObject("apartment door",
    "The familiar wooden slab, leading to a safe haven away from the "
    "judgment of strangers. You completely ignore the threatening mass of "
    "letters and junk mail jammed into the mail slot and lying on the floor.",
    "Escape!")

newspaper = GameObject("newspaper", "A lonesome, folded newspaper. Could be "
    "used as a makeshift bed. If you were desperate enough.")

def sleep_in_newspaper():
    cont.gs.say("It's really not comfortable but you can't think of a better "
        "option.\n\n"
        "After twisting and turning for a while you fall asleep.\n")

    sleep()
    cont.gs.pause(2)
    cont.gs.say("You spend a restless night in the hallway. Game over.")
    cont.gs.quit("Press any key to give up.")

newspaper.add_use("default", sleep_in_newspaper)

def get_home():
    if keys in cont.gs.inventory:
        cont.gs.move_to(main_room)
    else:
        cont.gs.say("You rattle the door for a bit and come to a "
            "sudden realization that you forgot your keys. Panic engulfs you "
            "while you try to figure out what to do.\n")
        cont.gs.say("You notice a newspaper a little ways down the "
            "hallway.")
        hallway.add_object(newspaper)
        cont.gs.decrement_score(1)

apartment_door.add_use("default", get_home)
hand.add_use("apartment door", get_home)
keys.add_use("apartment door", get_home)

hallway.add_object(neighbour)
hallway.add_object(door_to_street)
hallway.add_object(apartment_door)
