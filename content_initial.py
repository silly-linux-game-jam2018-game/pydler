#!/usr/bin/python3
# -*- coding: utf-8 -*-

from location import *
from gameobject import GameObject
from helper import *
from random import randint

cont = None

class Content:
    def __init__(self, gs):
        self.gs = gs
        global cont
        cont = self

        self.gs.say("You wake up with your face planted on a keyboard, "
            "your stomach rumbling. You are still extremely tired and yearn to "
            "continue your peaceful dreams.\n\n"
            "If this is your first time and everything seems a bit confusing, "
            "the command " + green("use brains") + " should get you started.")
        self.gs.say("[ Press any key to begin. ]")
        self.gs.pause()

        self.gs.inventory.append(eyes)
        self.gs.inventory.append(hand)
        self.gs.inventory.append(legs)
        self.gs.inventory.append(brains)

        # Load and play background music
        self.gs.load_music("enduser_r1_session.ogg")
        self.gs.play_music()

    def get_gs(self):
        return self.gs

    def enter(self):
        self.gs.move_to(main_room)

def sleep():
    score = cont.gs.score
    jingle = None
    if score > 900:
        cont.gs.say("You feel heavenly glory descend upon yourself. You've "
            "joined Stallman in his heroic quest to rid the world of the "
            "evils of Proprietary Software.")
        jingle = "best.ogg"
    elif score > 3:
        cont.gs.say("For once you feel like a human being.")
        jingle = "best.ogg"
    elif score > -1:
        cont.gs.say("You feel nothing at all.")
        jingle = "neutral.ogg"
    elif score > -2:
        cont.gs.say("You dislike yourself.")
        jingle = "neutral2.ogg"
    elif score > -6:
        cont.gs.say("You hate yourself.")
        jingle = "neutral2.ogg"
    else:
        cont.gs.say("You loathe yourself and find existence painful.")
        if score > -6000:
            jingle = "dark.ogg"
    if jingle is not None:
        cont.gs.stop_music(1)
        tmp = GameObject("dummy", "")
        tmp.load_audio(jingle)
        tmp.play_audio(70)

### Global body objects

eyes = GameObject("eyes", "Window to your soul. Whatever that means.",
    "These orbs grant you the power of sight. Use them well.")
eyes.add_use("default", lambda : cont.gs.look_around())
# Catchall for item descriptions
eyes.add_use("", lambda target : cont.gs.say(target.desc))

legs = GameObject("legs", "Your preferred bipedal form of transportation.",
    "You wonder for a moment if it would be nice to have more than two.")
legs.add_use("default", "You dance a little jig.")

brains = GameObject("brains",
    "You can't actually see your brains, but you assume they're beautiful.",
    "Thinking about your thinking apparatus? Meta as all hell.")
brains.add_alias("brain")
brains.add_use("default", lambda : cont.gs.say(cont.gs.location.hint))
brains.add_use("", lambda target : cont.gs.say(target.hint))

hand = GameObject("hand", "Handy.",
    "You should trim your fingernails before it's too late.")
hand.add_alias("hands")
hand.add_use("default", "You can do that later on your own time.")
hand.add_use("hand",
    "You shake your right hand with your left. You feel like a winner.")
hand.add_use("eyes", "You try to rub the sleep out of your eyes. The sleep "
    "sneers at your feeble attempt and clings on.")
hand.add_use("legs", "You poke at your legs with your fingers. They feel "
    "approximately like legs.")
hand.add_use("brains", "You dig one finger deep in your right ear and another "
    "in your left nostril. You feel something wet and mushy in both orifices, "
    "but that's probably not your brain.")
