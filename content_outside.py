#!/usr/bin/env python
# -*- coding: utf-8 -*-
#


### Game locations
# Note that naming policy is different from objects. The basic
# requirement is that the name works after "You are in ".

apartment_street = Location("front of your apartment complex",
    "The sun you've not seen in days sears your eyes and the dust kicked up "
    "by passing vehicles hurts your lungs. Formless, rushing blobs crowd the "
    "walkway and you hesitate to join the flow.\n\n"
#    "Not many cars around. I guess people who can't afford a better "
#    "apartment aren't likely to own a motor vehicle.\n\n"
    "There are a few rusty wrecks of what once were bicycles chained to a "
    "railing next to the door. None of them yours.")
bridge = Location("the middle of a bridge",
    "The bridge spans the top of a massive concrete dam covered with graffiti. "
    "There's something majestic about this grey, hulking structure but you "
    "can't quite figure out why you feel that way.\n\n"
    "The water flows freely through the floodgates to keep the river from "
    "overflowing and drowning the surroundings.\n\n"
    "The steep precipice makes you have thoughts you should not have.")
park = Location("a park",
    "The pathway into the park is lined with large pines, finally providing "
    "the shade you've been yearning for. The field opening in front of you "
    "has wide patches of lawn surrounding an area with larger things moving "
    "tiny bipedal blobs back and forth, up and down.\n\n"
    "At least it's not so crowded here, but your ears start to hurt from the "
    "piercing yelps and screams emanating from the overgrown fetuses.")

jail = Location("a jail cell",
    "The cell is cramped and largely uninteresting. Kind of like your "
    "apartment, actually. There's no obvious escape from here.")

hell = Location("Hell",
    "Hellish sights unfold in front of your eyes in all directions. "
    "And you don't even have a super-shotgun to aid you. Drat.",
    "Maybe you should have tried that before you ended up here.")

# TODO:
# riverbed (not necessary, no ideas for that shit)

### Objects
# Use lowercase for the names and make them unique and preferably easy
# to type. If a thing - like a door - can be seen from two locations,
# it needs to be two separate objects.

# Street

apartment_street.load_audio("street.ogg")

def street_on_enter():
    apartment_street.play_audio()
    if not cont.gs.get_flag("first entered street"):
        if not cont.gs.get_flag("clothed"):
            msg = "Your attire — or rather the lack of one — nets you " + \
                "shocked looks and a wide berth." + \
                (" At least they won't get close enough to detect your stench.", "")[int(cont.gs.get_flag("showered"))]
            cont.gs.say(msg)
            cont.gs.decrement_score(1)
        elif not cont.gs.get_flag("showered"):
            cont.gs.say("The passersby scrunch up their noses due to your stench.")
            cont.gs.decrement_score(1)
        cont.gs.set_flag("first entered street")

    if randint(1, 3) == 2:
        cont.gs.say("A car speeds by on its journey to places more interesting.")
apartment_street.on_enter = street_on_enter
apartment_street.on_leave = lambda : cont.gs.shut_up(apartment_street.channel, 1)

litter = GameObject("piece of litter",
    "One man's trash is another's treasure. This is just trash though.",
    "You're certainly no clean freak, but leaving trash on the streets "
    "always rubbed you the wrong way.")
litter.add_alias("litter")

garbage_can = GameObject("trashcan",
    "A local, round garbage-receptacle.",
    "You ponder for a moment what you could put in there... You probably "
    "wouldn't fit. For some reason that makes you a bit sad.")
garbage_can.add_use("default", "You give the side of the container a little " +\
    "kick with the tip of your toe. It sound it makes is as dull as your life.")

def fish_for_treasures():
    if cont.gs.get_flag("litter in trashcan"):
        cont.gs.say("You rediscover the piece of litter you put into "
            "the bin earlier. You decide to just leave it.")
        cont.gs.decrement_score(1)
    else:
        cont.gs.say("Your attempt to find something is commendable. "
            "Alas, the trashbin is empty.")
        cont.gs.decrement_score(1)

hand.add_use("trashcan", fish_for_treasures)

def put_trash_in_trashcan():
    litter = cont.gs.get_accessible_object("piece of litter")

    if litter == None:
        cont.gs.say("The piece of litter is already gone.")
    else:
        if litter in cont.gs.inventory:
            cont.gs.inventory.remove(litter)
        elif litter in cont.gs.location.objects:
            cont.gs.location.objects.remove(litter)
        cont.gs.say("You put the piece of litter into the trashcan. "
            "While this action may not have saved the planet, it has made "
            "the street a bit more tolerable to look at.")
        cont.gs.increment_score(1)
        cont.gs.set_flag("litter in trashcan")

litter.add_use("trashcan", put_trash_in_trashcan)

door_to_apartment_complex = GameObject("building front door",
    "This door leads back into the dark depths of the apartment complex.")
door_to_apartment_complex.add_alias("front door")
door_to_apartment_complex.add_alias("door")

hand.add_use("building front door", lambda : cont.gs.move_to(hallway))
door_to_apartment_complex.add_use("default", lambda : cont.gs.move_to(hallway))
keys.add_use("building front door",
    "Your key should fit, but the door isn't locked during the day.")

gate_to_store = GameObject("store gate",
    "It's a gate in the wire mesh fence enclosing the yard of the local "
    "convenience store.",
    "How are you even supposed to indicate a path to another \"room\" in "
    "an open environment? Eh, a gate will do.")
gate_to_store.add_alias("gate")

hand.add_use("shop gate", lambda : cont.gs.move_to(store_yard))
gate_to_store.add_use("default", lambda : cont.gs.move_to(store_yard))
legs.add_use("shop gate", lambda : cont.gs.move_to(store_yard))

path_to_bridge = GameObject("bridge",
    "It's a bridge that leads towards a city park.")

legs.add_use("bridge", lambda : cont.gs.move_to(bridge))
path_to_bridge.add_use("default", lambda : cont.gs.move_to(bridge))

bystander = GameObject("person", "Just your typical neighbourhood biped. "
    "It stands at the street corner, browsing social media on its phone.",
    "Addicted to social media. Easily the most loathsome of all media.")

def use_bystander():
    cont.gs.say("You attempt to fondle the biped but it recoils at "
        "your attempt and dodges. Clearly its DV is much too high for you.")
    cont.gs.say("It looks at you with total disgust.")
    cont.gs.decrement_score(1)

bystander.add_use("default", use_bystander)
hand.add_use("person", use_bystander)

def kick_bystander():
    cont.gs.say("You apply a friendly energy transfer from your leg "
        "to the person's backend. The person drops their phone and its screen "
        "shatters on impact with the ground. The person is not amused and "
        "stomps off in a storm of threats and curses.")
    apartment_street.remove_object(bystander)
    cont.gs.decrement_score(1)

legs.add_use("person", kick_bystander)

def slice_up_person():
    cont.gs.say("You take the razor and slice it across the biped's "
        "frail body multiple times in quick succession. After a while all you "
        "are left with is a bloody pulp of a lifeless corpse.")
    cont.gs.decrement_score(5)
    cont.gs.pause(1)
    cont.gs.say("The cops arrive in record time and arrest you. "
        "You are taken to the police station for holding.")
    cont.gs.move_to(jail)

razor.add_use("person", slice_up_person)
wallet.add_use("person",
    "They might be standing on the street, but they're not offering. And you "
    "couldn't afford it even if they were.")


apartment_street.add_object(bystander)
apartment_street.add_object(litter)
apartment_street.add_object(garbage_can)
apartment_street.add_object(door_to_apartment_complex)
apartment_street.add_object(gate_to_store)
apartment_street.add_object(path_to_bridge)

# Bridge

bridge.load_audio("damloop.ogg")

path_to_street = GameObject("street", "Way to your apartment.")

legs.add_use("street", lambda : cont.gs.move_to(apartment_street))
path_to_street.add_use("default", lambda : cont.gs.move_to(apartment_street))

path_to_park = GameObject("pathway",
    "A relaxing, narrow pathway towards the park near your apartment.")

legs.add_use("pathway", lambda : cont.gs.move_to(park))
path_to_park.add_use("default", lambda : cont.gs.move_to(park))

bridge_railing = GameObject("railing",
    "A waist high metal fence that keeps people from accidentally plummeting "
    "to their demise.", "Better stay away from it.")
bridge_railing.add_use("default", "You lean precariously over the railing, "
    "studying the large rocks at the bottom of the dam wall. They don't "
    "seem to care.")

def plummet_to_your_doom():
    cont.gs.say("You hoist yourself onto the railing and lean forward...")
    cont.gs.pause(2)
    cont.gs.say("You're falling, you're falling...")
    cont.gs.pause(2)
    cont.gs.say("SPLAT! The rocks below catch you in their cold and "
        "hard embrace. Every bone in your body snaps or cracks in a cacophony "
        "of unrecoverable injury. You sink into the shallow water as life "
        "escapes your body.")
    cont.gs.say("You are dead. The end.")
    cont.gs.quit("Press any key to regret your choices.")

legs.add_use("railing", plummet_to_your_doom)

hand.add_use("railing", 
    "You feel the cold metal of the railing pressing against your palm.")

bridge.add_object(path_to_street)
bridge.add_object(path_to_park)
bridge.add_object(bridge_railing)

bridge.on_enter = lambda : bridge.play_audio(40, AUDIO_LOOP, 1)
bridge.on_leave = lambda : cont.gs.shut_up(bridge.channel, 2)

# Park

park.load_audio("birbs.ogg")

def get_out_and_exercise_lazy_scumbag():
    park.play_audio()
    if not cont.gs.get_flag("taken hike"):
        cont.gs.say("You remember that it's been a while since you've "
            "actually really walked any significant distances outside. And "
            "as it turns out, coming out here feels kind of nice.")
        cont.gs.increment_score(1)
        cont.gs.set_flag("taken hike")
    else:
        cont.gs.say("Here you are again!")
    cont.gs.unset_flag("looked")

park.on_enter = get_out_and_exercise_lazy_scumbag
park.on_leave = lambda : cont.gs.shut_up(park.channel, 1)

children = GameObject("pack of dwarfs",
    "Small bipeds are running around the park, screaming.")
children.add_alias("pack of dwarves")
children.add_alias("dwarfs")

def no_using_children():
    cont.gs.say("The children run away from you when you approach.")
    park.remove_object(children)
    cont.gs.decrement_score(1)
    cont.gs.say("You draw some " + underline("very judgmental") + " looks.")

children.add_use("default", no_using_children)
hand.add_use("pack of dwarfs", no_using_children)

wallet.add_use("pack of dwarfs", 
    "As nice as that might be, don't you think you might need that money more? "
    "Not to mention that they would probably just buy candy with that money "
    "and ruin their teeth.")

def stare_at_children():
    if not cont.gs.get_flag("looked"):
        cont.gs.say("Small bipeds are running around the park, screaming loudly.")
        cont.gs.set_flag("looked")
    elif not cont.gs.get_flag("stared"):
        cont.gs.say("One of the overgrown fetuses stops running and turns to look at you.")
        cont.gs.set_flag("stared")
    elif not cont.gs.get_flag("scared"):
        cont.gs.say("The children scatter and hide from your incessant stare.")
        park.remove_object(children)
        cont.gs.set_flag("scared")
        cont.gs.decrement_score(1)
    else:
        cont.gs.say("They are no longer in sight.")

eyes.add_use("pack of dwarfs", stare_at_children)

def oh_the_humanity_you_terrible_monster():
    cont.gs.say("You implant the razor into the throat of one of the "
        "children and it begins to bleed out.\n\nYour LOVE increased.\n\n"
        "Just as you start your second lunge towards the terrified children "
        "a shot rings out and you feel a sting in your back. You fall hard onto "
        "the ground as your feet stop functioning, then roll over to your back.\n\n"
        "Before your sight fades you see a police officer running at you, gun in hand.")
    cont.gs.decrement_score(6666)
    cont.gs.say("[ Press any key to give up the ghost. ]")
    cont.gs.pause()
    cont.gs.move_to(hell)

razor.add_use("pack of dwarfs", oh_the_humanity_you_terrible_monster)

bridge_back = GameObject("bridge back",
    "It's the bridge leading back towards your apartment complex.")
bridge_back.add_alias("bridge")
bridge_back.add_use("default", lambda : cont.gs.move_to(bridge))
legs.add_use("bridge back", lambda : cont.gs.move_to(bridge))

park.add_object(bridge_back)
park.add_object(children)

# Jail
bed_of_long_punishment = GameObject("bed",
    "It's a typical prison bed. Obviously nothing fancy, but still probably " +\
    "more comfortable than your couch.")

def sleep_in_bed_of_long_punishment():
    cont.gs.say("You lie down on the prison bed. Yes, it's definitely more "
        "comfortable than your couch. And it better be, because you won't "
        "be leaving in a hurry.")
    sleep()
    cont.gs.say("You'll serve a long prison sentence for your crimes. Game over.")
    cont.gs.quit("Press any key. Do it!")

bed_of_long_punishment.add_use("default", sleep_in_bed_of_long_punishment)

jail.add_object(bed_of_long_punishment)

# Hell (you don't want to come here)

hell.load_audio("dark.ogg")

bed_of_eternal_torment = GameObject("bed",
    "It's the bed of eternal torment. Sharp, black thorns stick out from "
    "the mattress and pillow. The blanket is visibly crawling with various "
    "insects you can't even recognize.",
    "You made your bed. Now lie in it.")

def sleep_in_bed_of_eternal_torment():
    cont.gs.say("You lie down on the bed of eternal torment. It's not "
        "very comfortable.\n\n"
        "After what feels like an eternity of pain and suffering you fall into "
        "a troubled sleep, haunted by dreams that instil a deep sense of "
        "self-hatred. You might now take a different path if given the "
        "possibility.")

    sleep()

    cont.gs.say("You spend the rest of eternity in Hell. Game over.")
    cont.gs.quit("Press any key to suffer forever.")
bed_of_eternal_torment.add_use("default", sleep_in_bed_of_eternal_torment)
hand.add_use("bed",
    "You poke the barbed tip of one of the thorns seemingly growing out of "
    "the rock-hard pillow. It pokes back, painfully.")
razor.add_use("bed",
    "Every time you cut off a thorn or kill an insect, something even more "
    "vicious immediately takes its place. You know it's too late for "
    "complaints, but that seems a bit unfair.")

def go_to_hell():
    cont.gs.stop_music(1)
    hell.play_audio()

hell.on_enter = go_to_hell
hell.add_object(bed_of_eternal_torment)
