#!/usr/bin/env python
# -*- coding: utf-8 -*-
#


### Game locations
# Note that naming policy is different from objects. The basic
# requirement is that the name works after "You are in ".

store_yard = Location(
    "the convenience store parking lot",
    "Your eyes behold a vista of multiple square metres of potholed paved yard "
    "filled with rusting motor transport. Broken lamp posts have wheels of "
    "bicycles chained to them, speaking volumes of the kinds of people who "
    "frequent this slice of heaven.\n\n"
    "All this glory is lit by enormous neon signs, all flashing the word " +
    blink("STORE") + " in a seemingly random pattern, overpowering "
    "the sun with their brightness. You think there should be one of those "
    "warning placards for people with photosensitive epilepsy at the gate.")

store = Location("an inconvenient convenience store",
    "The first thing your eyes spot is the counter with a person seemingly "
    "rooted behind it. Next to it stands an intimidating staff member "
    "following your every movement.\n\nYour anxiety gets worse.\n\n"
    "Your eyes glide over the shelves containing things to eat and things that "
    "might be useful for taping someone's mouth shut. The place is unkempt, "
    "like it hasn't seen a cleaning since it was built. Almost like your "
    "apartment.",
    "This establishment is what someone might call a store if they were "
    "feeling unduly generous. It sells things. Some of those things should "
    "serve to sate your hunger.")

back_alley = Location("dark corners of the Earth",
    "Shady backside of the shady store. The acrid smell of human urine "
    "invades your nostrils. The alley is dominated by massive dumpster, open "
    "and inviting you to dive.\n\n"
    "There's also the seemingly abandoned dwelling of some unfortunate person, "
    "constructed out of a single cardboard box.\n\n"
    "Your options seem limitless.")


### Objects
# Use lowercase for the names and make them unique and preferably easy
# to type. If a thing - like a door - can be seen from two locations,
# it needs to be two separate objects.

#store yard
gate_to_street = GameObject("gate", "A gate you don't feel happy to be on "
    "this side of. You are anxious to rush through it and back home.",
    "Run back! Home! Please?")
gate_to_street.add_use("default", lambda : cont.gs.move_to(apartment_street))
hand.add_use("gate", lambda : cont.gs.move_to(apartment_street))
legs.add_use("gate", lambda : cont.gs.say("It makes a massive noise. Your foot hurts."))

way_around = GameObject("path around",
    "The way to the back of the store. You assume that side is even worse.",
    "The paved path visibly skirts the wall of the building and continues into "
    "an alleyway around the corner. If you follow the path, an alley behind "
    "the store is where you'll indubitably end up.")
way_around.add_alias("path")
legs.add_use("path around", lambda : cont.gs.move_to(back_alley))
way_around.add_use("default", lambda : cont.gs.move_to(back_alley))

store_door = GameObject("ENTRY", "A barely functional pair of sliding doors. "
    "They mew and whimper like abandoned kittens as they open and close "
    "continuously thanks to broken sensors.",
    "The only way to feed yourself lies inside.")
legs.add_use("ENTRY", lambda : cont.gs.move_to(store))
hand.add_use("ENTRY",
    "You wave your hand in front of the sensor. This has no visible effect on "
    "the relentless dance of the sliding doors.")
store_door.add_use("default", lambda : cont.gs.move_to(store))
store_door.add_alias("door")

signs = GameObject("signs",
    "These beasts must be visible from space. You stand transfixed - gazing up "
    "at them in stupefied awe - for the couple of seconds until you're "
    "forced to turn away by the pain in your eyeballs.",
    "Even thinking about them hurts your eyes. And your brain.")
signs.add_alias("sign")
hand.add_use("signs",
    "The signs are out of reach. Probably a good thing; Their heat would burn "
    "your skin just as easily as their light sears your retinas.")

store_yard.add_object(store_door)
store_yard.add_object(gate_to_street)
store_yard.add_object(way_around)
store_yard.add_object(signs)

#behind store
way_to_front = GameObject("store front", "Quick escape back to the pleasant "
    "store yard.", "You should go there.")
legs.add_use("way front", lambda : cont.gs.move_to(store_yard))
way_to_front.add_use("default", lambda : cont.gs.move_to(store_yard))

path_through_bushes = GameObject("hedge", "A thick hedge. It fails to dampen "
    "the screams of children beyond. The park must be over that way.",
    "You think you should be able to squeeze through.")
path_through_bushes.add_use("default", lambda : cont.gs.move_to(park))
legs.add_use("hedge", lambda : cont.gs.move_to(park))

hobo_home = GameObject("cardboard box", "The apartment of a biped even more "
    "unfortunate than you. You did not believe that to be possible. "
    "It reeks of urine, some of it still soaking the cardboard.",
    "You wouldn't sleep there, now would you?")
hobo_home.add_alias("box")
hobo_home.load_audio("boxcrumble.ogg")

def stomp_box():
    hobo_home.play_audio()
    cont.gs.say("You stomp on the cardboard box furiously. Still "
        "fresh urine splashes your legs. You have destroyed some person's "
        "home.\n\n"
        "You don't feel any better by your actions.")
    cont.gs.decrement_score(1)
    back_alley.remove_object(hobo_home)

legs.add_use("cardboard box", stomp_box)
hand.add_use("cardboard box",
    "As long as you fondle a bit that's not soaked through, the box feels "
    "exactly like cardboard. The soaked bits feel unpleasantly mushy.")
razor.add_use("cardboard box",
    "You cut a little sad face into the side of the sad little dwelling. You "
    "actually set out to carve a smiley face but this is how they always seem "
    "turn out these days.")

def sleep_in_hobo_hotel():
    cont.gs.say("You crawl inside the reeking cardboard box and curl into "
        "a ball. You have never felt more pathetic.\n\n"
        "After a while you fall asleep.")
    cont.gs.decrement_score(5)
    sleep()
    cont.gs.say("You sleep through the night. Game over.")
    cont.gs.quit("Press any key to fade away.")

hobo_home.add_use("default", sleep_in_hobo_hotel)

hobo_blanket = GameObject("blanket",
    "The piss-marinated rag someone has used to cover themselves. It's "
    "entirely and absolutely disgusting.",
    "What would you do with it?")

def bin_blankie():
    blanket = cont.gs.get_accessible_object("blanket")
    if blanket != None:
        if cont.gs.get_flag("pissrag"):
            cont.gs.increment_score(1)
            cont.gs.unset_flag("pissrag")
        if blanket in cont.gs.inventory:
            cont.gs.inventory.remove(blanket)
        elif blanket in cont.gs.location.objects:
            cont.gs.location.objects.remove(blanket)
        cont.gs.say("You throw the blanket where it belongs. Good riddance.")

def pick_rag():
    cont.gs.add_to_inventory(hobo_blanket)
    cont.gs.set_flag("pissrag")
    cont.gs.decrement_score(1)

hand.add_use("blanket", pick_rag)
hobo_blanket.add_use("dumpster", bin_blankie)
hobo_blanket.add_use("trashcan", bin_blankie)

dumpster = GameObject("dumpster", "Massive container filled with things "
    "the store is unable to sell. Some moldy, some still edible.",
    "The dumpster might provide an alternative source of questionable "
    "corporeal nourishment if you're desperate. Or if you've just given up. "
    "You spend a while pondering where you fall on this until you realise "
    "these two things are not mutually exclusive.")

garbage_food = GameObject("expired edibles",
    "Eating this will definitely probably maybe not make you sick.")
garbage_food.add_use("default", lambda : cont.gs.say("You take a bite, "
    "suppress a gag, then decide you might want to wait until you're just a bit "
    "more desperate before digging in."))

def dumpsterdive():
    if edibles not in cont.gs.inventory:
        if garbage_food not in cont.gs.inventory:
            cont.gs.say("You dive head first into the dumpster and "
                "easily score something passable to feed yourself with.\n\n"
                "After climbing out you are delighted to find your appearance "
                "relatively unmarred by the adventure.")
            cont.gs.location.add_object(garbage_food)
            cont.gs.add_to_inventory(garbage_food)
        else:
            cont.gs.say("You take another peek around the insides of "
                "the container.\n\n"
                "You don't think you need anything more from there; "
                "the edibles you currently possess should be sufficient.")
    else:
        cont.gs.say("You take a peek around the insides of the container.\n\n" +\
            "You don't see anything of interest; the edibles you currently "
            "possess should be sufficient.")
        

dumpster.add_use("default", dumpsterdive)
hand.add_use("dumpster", dumpsterdive)

alley_door = GameObject("metal door",
    "There used to be an access door from the store to this alley, but now "
    "it's just a rectangle of rusty metal in the concrete wall. An ugly weld "
    "seam runs around the edges. Someone obviously wanted this door to stay "
    "shut.\n\n"
    "The lower half of the door is covered in a pattern of black scribbles. "
    "Leaning close, you surmise that the blocky figures are actually tiny "
    "writing in a hieroglyphic language you can't make heads or tails of.")
alley_door.add_alias("door")
hand.add_use("metal door",
    "You scratch a loose flake of red spray paint off the metal surface with "
    "your fingernail, revealing a patch of older paint of a slightly different "
    "hue.")

back_alley.add_object(alley_door)
back_alley.add_object(hobo_home)
back_alley.add_object(hobo_blanket)
back_alley.add_object(dumpster)
back_alley.add_object(way_to_front)
back_alley.add_object(path_through_bushes)

#store

door_to_yard = GameObject("EXIT", "The same whining sliding doors you "
    "entered the store through.", "RUN AWAY")
door_to_yard.add_alias("door")

door_to_yard.add_use("default", lambda : cont.gs.move_to(store_yard))

def store_on_leave():
    if cont.gs.get_flag("stealyman") and not cont.gs.get_flag("paid"):
        if edibles in cont.gs.inventory:
            cont.gs.say("\nThe security hulk comes rushing after you, rips "
                "away everything you took, bans you from entering ever again "
                "and unceremoniously dumps you out front. What a bully.")
            cont.gs.inventory.remove(edibles)
        if not_so_edible in cont.gs.inventory:
            cont.gs.inventory.remove(not_so_edible)
        else:
            cont.gs.say("\nThe security hulk grabs you from behind and flings "
                "you through the door. You don't think you're welcome back "
                "anytime soon.")
        cont.gs.decrement_score(1)
        store_yard.remove_object(store_door)
        cont.gs.unset_flag("stealyman")
        cont.gs.say("[ Press any key to land painfully. ]")
        cont.gs.pause()

store.on_leave = store_on_leave

store_counter = GameObject("counter", "A rusted hunk of junk with an "
    "ancient cash register and a clerk - nearly as ancient - behind it. "
    "You can see a depthless disgust towards humankind in the eyes of "
    "the firmly rooted blob.",
    "You might be able to purchase things here.")

def slice_guts():
    cont.gs.say("You launch yourself towards the blob, suddenly inspired to "
        "see if you could loosen those roots just a bit. Midway to the "
        "supple guts of the clerk the unreasonably competent security "
        "detail tackles you to the floor. The menace proceeds to forcefully "
        "throw you out of the store, banning you for life.")
    cont.gs.decrement_score(1)
    store_yard.remove_object(store_door)
    cont.gs.say("[ Press any key to land painfully. ]")
    cont.gs.pause()
    cont.gs.move_to(store_yard)

def pay_up():
    if cont.gs.get_flag("stealyman"):
        cont.gs.say("You set the items on the counter and listen to the "
            "dreary dribble flowing out of the blob. After you deem it has "
            "finished, you absentmindedly allow it to bleed your debit card "
            "and think about running back home.\n\n"
            "Surely you can't need anything more.")
        cont.gs.unset_flag("stealyman")
        cont.gs.set_flag("paid")
    else:
        cont.gs.say("You wave your wallet in front of the blob but only "
            "get an annoyed grunt in response.")

wallet.add_use("counter", pay_up)
razor.add_use("counter", slice_guts)
hand.add_use("counter",
    "The counter feels sturdy and sticky. Mostly sticky. You have trouble "
    "extricating your fingers from the countertop.")
def kick_counter():
    cont.gs.say("You go for a swift kick but it turns into an awkward stumble "
        "when you remember you're being watched. Anxiety twists your insides, "
        "pressing heavily on your lungs.")
    cont.gs.decrement_score(1)
    legs.delete_use("counter")
legs.add_use("counter", kick_counter)

edibles = GameObject("edibles", "A shelf filled with a selection of things "
    "people stuff into their mouths when they are desperate.\n\n"
    "Along with cockroaches, these are the other thing that could survive a "
    "nuclear apocalypse. You are still unsure if you'd rather not just starve.",
    "You could use these to fill your empty fridge.")

def take_edibles():
    cont.gs.say("You spend a while coming up with and implementing a selection "
        "algorithm based on risk/reward and apply it to the selection of so "
        "called food on the shelves." + \
        ("\n", " You unceremoniously dump the dumpster-sourced stuff on the floor.\n")[garbage_food in cont.gs.inventory])
    if garbage_food in cont.gs.inventory:
        cont.gs.inventory.remove(garbage_food)
    cont.gs.set_flag("stealyman")
    cont.gs.add_to_inventory(edibles)

def eat_edibles():
    if not cont.gs.get_flag("ate"):
        if not cont.gs.get_flag("paid"):
            cont.gs.say("You stuff the edible looking things into your "
                "mouth hole with considerable vigour. You get disgusted looks "
                "from the blobs currently occupying the facility.\n\n"
                "The security detail indicates that you should pay for the "
                "things with great enthusiasm.")
            cont.gs.set_flag("stealyman")
            cont.gs.decrement_score(1)
        else:
            cont.gs.say("You stuff the edible looking things into your "
                "mouth hole with considerable vigour. All of them. You did pay "
                "for them, but maybe you could have saved some to put into your "
                "fridge?")
        cont.gs.set_flag('ate')
        if edibles in cont.gs.inventory:
            cont.gs.inventory.remove(edibles)
    else:
        cont.gs.say("You just ate. Any more would just make you feel "
            "sick. You decide to keep it for later.")

hand.add_use("edibles", take_edibles)
edibles.add_use("default", eat_edibles)

def store_edibles_in_fridge():
    cont.gs.say("You store your edible goodies in the fridge. You "
        "think you might survive on these for a while.")
    if edibles in cont.gs.inventory:
        cont.gs.inventory.remove(edibles)
    if garbage_food in cont.gs.inventory:
        cont.gs.inventory.remove(garbage_food)
    hand.add_use("fridge", lambda : cont.gs.say("The fridge actually "
        "contains food! You grab a quick snack and close the door."))
    cont.gs.increment_score(1)

edibles.add_use("fridge", store_edibles_in_fridge)
garbage_food.add_use("fridge", store_edibles_in_fridge)

not_so_edible = GameObject("junk", "A collection of necessary household "
    "items. Mostly items suitable for disposing of a corpse. Duct tape, "
    "shovels, plastic bags...", "You don't think you have any use for these.")

def tape_your_mouth_you_dirty_self_loathing_person():
    cont.gs.say("You reach for the nearest roll of duct tape and tape "
            "your mouth shut. You feel like you should have been born this way.")
    cont.gs.set_flag("stealyman")
    cont.gs.decrement_score(1)

not_so_edible.add_use("default", tape_your_mouth_you_dirty_self_loathing_person)
hand.add_use("junk", lambda : cont.gs.say("You'd rather not take any of those things."))

store_back_door = GameObject("back door",
    "A windowless metal door, more or less opposite the one you came in "
    "through. The wide handle has been crudely sawed off and the door itself "
    "looks welded shut. No way to open it. You guess that means it's "
    "technically not even a door anymore. Just a rectangular slab of metal "
    "embedded in the back wall.")

store.add_object(store_back_door)
store.add_object(door_to_yard)
store.add_object(store_counter)
store.add_object(edibles)
store.add_object(not_so_edible)
