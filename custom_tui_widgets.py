# customized widgets
from picotui.widgets import *
from picotui.defs import *
from picotui.basewidget import *
from helper import strip_ansi
import sys

class VScrollBar(FocusableWidget):
    def __init__(self, h, max_value, value, sibling = None):
        self.h = h
        self.w = 1
        self.sibling = sibling
        self.max_value = max_value
        self.value = value
        super().__init__()


    def set_sibling(self, sibling):
        self.sibling = sibling


    def set_maxvalue(self, max_value):
        if self.max_value != max_value:
            self.max_value = max_value
            self.redraw()


    def set_value(self, value):
        if self.value != value:
            self.value = value
            self.redraw()


    def redraw(self):
        self.show_vertical_scrollbar()


    def show_vertical_scrollbar(self):
        self.cursor(False)
        top = self.y
        height = self.h
        bottom = top + height
        x = self.x
        self.goto(x, top)
        self.wr('▲')
        top += 1
        while top < bottom - 1 :
            self.goto(x, top)
            self.wr('░')
            top += 1
        self.goto(x,  top)
        self.wr('▼')

        pos_indicator = 0
        if(self.max_value) > 0:
            len_scroll = height - 2
            pos_indicator_factor = float(len_scroll) / float(self.max_value)
            pos_indicator = round(pos_indicator_factor * self.value)

        if not (pos_indicator < 0 or pos_indicator > height - 1):
            y = (self.y + 1) + pos_indicator
            self.goto(x, y)
            self.wr('*')


    def handle_mouse(self, x, y):
        if self.sibling is not None:
            if y == self.y:
                self.sibling.handle_key(KEY_UP)
            elif y == self.y + self.h -1:
                self.sibling.handle_key(KEY_DOWN)
            elif y < self.y + self.value:
                self.sibling.handle_key(KEY_PGUP)
            elif y > self.y + self.value:
                self.sibling.handle_key(KEY_PGDN)


    def handle_key(self, key):
        #TODO
        pass


class TextBox(WMultiEntry):
    def __init__(self, w, h, lines,  vscrollbar = None):
        self.vscrollbar = vscrollbar
        WMultiEntry.__init__(self, w, h, lines)
        if vscrollbar is not None:
            vscrollbar.set_sibling(self)
            self.set_scrollbar_values()


    def set_scrollbar_values(self):
        if self.vscrollbar is not None:
            self.vscrollbar.set_maxvalue(len(self.content))
            self.vscrollbar.set_value(self.cur_line)


    def handle_key(self, key):
        if key == KEY_QUIT:
            return key
        self.handle_cursor_keys(key)
        if self.vscrollbar is not None:
            self.set_scrollbar_values()
        return


    def set_cursor(self):
        if self.vscrollbar is not None:
            self.set_scrollbar_values()
        super().set_cursor()


    def show_line(self, l, i):
        l = l[self.margin:]
        achars = len(l) - len(strip_ansi(l, "#")[0])
        l = l[:self.width + achars]
        self.wr(l)
        self.clear_num_pos(self.width + achars - len(l))


    def redraw(self):
        super().redraw()


class MyCompletionList(WCompletionList):
    def __init__(self, x, y, w, h, items):
        Dialog.__init__(self, x, y, w, h)
        self.list = self.OneShotList(w - 2, h - 2, items)
        self.add(1, 1, self.list)
        self.list.set_lines(items)
        self.list.top_line = 0
        self.list.cur_line = 0
        self.list.row = 0
        self.list.redraw()


class MyCommandLine(WAutoComplete):

    popup_class = MyCompletionList

    def __init__(self, w, text, sibling = None):
        self.sibling = sibling
        self.prev = None
        WAutoComplete.__init__(self, w, text, [])


    def set_sibling(self, sibling):
        self.sibling = sibling


    def handle_key(self, key):
        if self.sibling is not None:
            if key == KEY_UP:
                if self.prev:
                    self.set(self.prev)
                self.focus = True
                self.col = len(self.get())
                self.redraw()
                return True
            if key == KEY_PGDN:
                self.sibling.handle_key(KEY_PGDN)
                self.focus = True
                self.redraw()
                return True
            if key == KEY_PGUP:
                self.sibling.handle_key(KEY_PGUP)
                self.focus = True
                self.redraw()
                return True
        return super().handle_key(key)


    def show_popup(self):
        text = self.get().rsplit(" ", 1)[-1]
        choices = self.get_choices(text, True)
        if len(choices) == 0:
            return
        elif len(choices) > 1:
            height = min(len(choices), 17)
            popup = self.popup_class(self.x, self.y - height - 2, self.longest(choices) + 2, height + 2, choices)
            popup.main_widget = self
            res = popup.loop()
            if res == ACTION_OK:
                val = popup.get_selected_value()
                if val is not None:
                    self.set_lines([self.get() + val[len(text):]])
        else:
            self.set(self.get() + choices[0][len(text):])
        self.col = sys.maxsize
        self.adjust_cursor_eol()
        self.just_started = False
        self.owner.redraw()


    def handle_edit_key(self, key):
        if key == KEY_ENTER:
            self.prev = self.get()
            self.signal("enter_used")
            return True
        elif key != KEY_ESC:
            return super().handle_edit_key(key)


class MyList(WListBox):
    def __init__(self, w, h, items):
        super().__init__(w, h, items)

    def show_line(self, l, i):
        hlite = self.cur_line == i
        if hlite:
            if self.focus:
                self.attr_color(C_B_WHITE, C_GREEN)
        if i != -1:
            l = self.render_line(l)[:self.width]
            self.wr(l)
        self.clear_num_pos(self.width - len(l))
        if hlite:
            self.attr_reset()

    def handle_mouse(self, x, y):
        if y <= self.top_line + self.total_lines + self.y - 1:
            line = y - self.top_line - self.y
            self.cur_line = line
            self.choice = line
            self.signal("clicked")
            return True
        else:
            self.signal("misclick")
            return True

    def handle_edit_key(self, key):
        if key == KEY_ENTER:
            self.choice = self.cur_line
            self.signal("clicked")
            return True
        else:
            self.signal("misclick")
            return True


class MyLabel(WLabel):
    def __init__(self, text, w, fg = C_WHITE, bg = None):
        super().__init__(text, w)
        self.fg = fg
        self.bg = bg

    def redraw(self):
        self.goto(self.x, self.y)
        self.attr_color(self.fg, self.bg)
        self.wr_fixedw(self.t, self.w)
        self.attr_reset()


class MyDialog(Dialog):
    def __init__(self, x, y, w=0, h=0, title=""):
        super().__init__(x, y, w, h, title)
        self.quit = False

    def endloop(self):
        self.quit = True

    def loop(self):
        self.redraw()
        while not self.quit:
            key = self.get_input()
            res = self.handle_input(key)

            if res is not None and res is not True:
                return res

        return 0
