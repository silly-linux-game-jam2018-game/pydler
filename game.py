from content import Content
from helper import *
import re

class MyGame:
    def __init__(self, gs):
        self.gs = gs
        self.cont = Content(self.gs)

    def first_location(self):
        """Enter the first location"""
        self.cont.enter()

    def parse_command(self, command):
        if len(command) > 0:
            # Remove articles and extra whitespace
            command = re.sub(r"\b(a|an|the)\b", "", command, flags = re.IGNORECASE)
            command = ' '.join(command.split())

            try:
                [verb, objs] = command.split(" ", 1)
                if " on " in objs:
                    objs = objs.split(" on ", 1)
                else:
                    objs = objs.split(" with ", 1)
            except:
                verb = command.strip()
                objs = []
            if verb == "quit":
                self.gs.say('You run out of stamina and eject prematurely.')
                self.gs.quit("Press any key to feel like a total loser.")
            if verb == "use":
                if len(objs) == 0:
                    self.gs.say("Use what?")
                if len(objs) == 1:
                    gob = self.gs.get_accessible_object(objs[0])
                    if gob == None:
                        self.gs.say("What is this {} you speak of?".format(objs[0]))
                    else:
                        res = gob.use("default")
                        if res:
                            self.gs.say(res)
                if len(objs) == 2:
                    gob = self.gs.get_accessible_object(objs[0])
                    target = self.gs.get_accessible_object(objs[1])

                    if gob == None and target == None:
                        self.gs.say("No such objects in sight.")
                    elif objs[0] == "eyes" and (objs[1] == "self" or objs[1] == "inventory"):
                        if len(self.gs.inventory) == 0:
                            self.gs.say("You are devoid of earthly possessions.")
                        elif len(self.gs.inventory) == 1:
                            self.gs.say("You are carrying a {}.".format(gs.inventory[0].name))
                        else:
                            self.gs.say(
                                "You have currently stashed about your person:\n" +
                                yellow("☛ " + (", ☛ ".join([obj.name for obj in self.gs.inventory]))))

                    elif gob == None:
                        self.gs.say("What is this {} you speak of?".format(objs[0]))
                    elif target == None:
                        self.gs.say("What is this {} you speak of?".format(objs[1]))
                    else:
                        if target.name.lower() in gob.uses.keys():
                            res = gob.use(target.name.lower())
                            if res:
                                self.gs.say(res)
                        else:
                            res = gob.use("", target)
                            if res:
                                self.gs.say(res)

            else:
                self.gs.say("I don't know what that means.")

        else:
            self.gs.say("HINT: Use finger on keyboard. Or \"quit\" if you're a quitter.")
