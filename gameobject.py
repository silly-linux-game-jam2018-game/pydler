#!/usr/bin/python3
# -*- coding: utf-8 -*-

from helper import *
audio_enabled = False
try:
    from sdl2.sdlmixer import *
    import ctypes
    if Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) != 0:
        audio_enabled = False
    else:
        Mix_Volume(-1, MIX_MAX_VOLUME)
        audio_enabled = True
    Mix_CloseAudio()
except:
    MIX_MAX_VOLUME = 128

class GameObject:
    """Base class for all game objects and items."""
    def __init__ (self, name, desc, hint = ""):
        self.name = name
        self.desc = desc
        self.sfx = None
        self.aliases = []
        if len(hint) > 0:
            self.hint = hint
        else:
            self.hint = "You can't think of any use for the {} right now.".format(self.name.lower())
        self.uses = {}

    def load_audio (self, filename):
        """
        Load audio file <filename> from "audio/" and set it up for playback.
        Fail silently if sdlmixer couldn't be imported.
        """
        if audio_enabled:
            try:
                self.sfx = Mix_LoadWAV(("audio/" + filename).encode("utf-8"))
            except:
                #TODO: Should be logged, not printed
                print("ERROR: Audio file " + filename + " could not be loaded.")
        return False

    def play_audio (self, volume = MIX_MAX_VOLUME):
        """
        Play previously set audio file at specified volume, defaults to maximum.
        Fails silently if file hasn't been loaded.
        """
        if audio_enabled:
            if self.sfx is not None:
                Mix_VolumeChunk(self.sfx, volume)
                Mix_PlayChannel(-1, self.sfx, 0)
        return False

    def add_use (self, obj, func):
        """
        Add a use target for object <obj>.
        The parameter <func> can be a function or a string.
        """
        self.uses[obj.lower()] = func

    def delete_use (self, obj):
        """
        Remove use target for object <obj>.
        Returns the removed function or string, or None if key doesn't exist.
        """
        return(self.uses.pop(obj.lower(), None))

    def use (self, obj, arg=None):
        obj = obj.lower()
        if obj in self.uses.keys():
            if arg == None:
                if isinstance(self.uses[obj], str):
                    return(self.uses[obj])
                else:
                    self.uses[obj]()
            else:
                if isinstance(self.uses[obj], str):
                    return(self.uses[obj](arg))
                else:
                    self.uses[obj](arg)
        elif "" in self.uses.keys() and arg != None:
            if isinstance(self.uses[""], str):
                return(self.uses[""](arg))
            else:
                self.uses[""](arg)
        else:
            if arg == None:
                return("You fondle the {} for a bit.".format(self.name, self.name))
            else:
                return("Using the {} on the {} is a bad idea and you should be ashamed.".format(self.name, arg.name))

    def add_alias (self, alias):
        """
        Adds an alias for the object.
        """
        if alias not in self.aliases:
            self.aliases.append(alias)
        return False
