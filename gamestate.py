#!/usr/bin/python3
# -*- coding: utf-8 -*-

from location import Location
from shutil import get_terminal_size
from helper import *

try:
    from sdl2.sdlmixer import *
    import ctypes
except:
    pass

class GameState:
    def __init__(self, audio = False):
        self.location = None
        #This flag tells cli UI to print location info.
        self.location_changed = False
        self.inventory = []
        self.story = []
        self.score = 0
        self.flags = {}
        self.music = None
        self.music_volume = 64
        if audio:
            Mix_VolumeMusic(self.music_volume)
        self.audio_enabled = audio

    def clear_story(self):
        """
        Clears current story.
        """
        self.story = []
        self.location_changed = False
        return False

    def say(self, content):
        """
        Appends <content> to current story. Can be a string or an integer
        amount of seconds to pause. 0 means pause for input, -1 means quit.
        """
        if isinstance(content, str):
            for line in content.split("\n\n"):
                self.story.append(line)
        elif isinstance(content, int):
            self.story.append(content)
        return False

    def pause(self, duration = None):
        """
        Convenience method to add a pause of <duration> seconds to the story.
        If <duration> is omitted or not a positive integer, wait for keypress.
        """
        if isinstance(duration, int) and duration > 0:
            self.say(duration)
        else:
            self.say(0)
        return False

    def add_to_inventory(self, obj, finite = True):
        """
        Add object to inventory.
        Remove from current location unless optional parameter finite = False.
        """
        if obj not in self.inventory:
            if obj in self.location.objects:
                self.inventory.append(obj)
                if finite:
                    self.location.objects.remove(obj)
                self.say("You pick up the {}.".format(obj.name))
        else:
            self.say("You are already carrying the {}.".format(obj.name))
        return False

    def get_accessible_object(self, name):
        """
        Returns a GameObject instance if <name> matches one (or an alias) in
        the inventory or current location.
        """
        for i in self.inventory:
            if name.lower() in map(str.lower, (i.aliases + [i.name])):
                return i

        for o in self.location.objects:
            if name.lower() in map(str.lower, (o.aliases + [o.name])):
                return o

        return None

    def object_table(self, objects, heading = ""):
        """
        Pretty-prints the list of objects in columns.
        The optional <heading> is included before the list.
        """
        maxlen = len(max(objects, key=len)) + 1
        maxcol = int((get_terminal_size()[0]) / maxlen) - 2
        col = 0
        text = heading
        for obj in objects:
            text += "☛ {:<{length}}  ".format(obj, length=str(maxlen))
            col += 1
            if col >= maxcol:
                col = 0
                text += "\n"
        if col < maxcol and len(objects) % maxcol != 0:
            text += "\n"
        self.say(text)
        return False

    def object_list(self, objects, heading = ""):
        """
        Returns a list of objects with icons, suitable for wrapping.
        The optional <heading> is included before the list.
        """
        if heading != "":
            self.say(heading)
        self.say(yellow("☛ " + ", ☛ ".join([obj for obj in objects])))
        return False

    def look_around(self):
        """Describes the current location and its contents."""
        self.say(self.location.desc)
        if len(self.location.objects) > 0:
            if len(self.location.objects) == 1:
                self.say("You notice a {}.".format(self.location.objects[0].name))
            else:
                self.say("Various things catch your interest:")
                self.say(yellow("☛§" + " ☛§".join([obj.name.replace(' ', '§') for obj in self.location.objects])))
        self.location_changed = True
        return False

    def move_to(self, loc):
        """
        Switches current location.
        Parameter <loc> should be an instance of the Location class.
        """
        if isinstance(loc, Location):
            if self.location is not None:
                self.location.run_on_leave()
            self.location = loc
            self.look_around()
            self.location.run_on_enter()
            self.location_changed = True
        else:
            self.say("Invalid location. Feel free to kick a game designer.")

        return False

    def describe_object(self, gob):
        """
        Describes the object.
        The parameter can be the name of the object or a GameObject instance.
        """
        if isinstance(gob, str):
            gob = self.get_accessible_object(obj)

        if gob != None:
            self.say(gob.desc)

    def increment_score(self, amount = 1):
        """
        Increments score by <amount>.
        """
        self.score += amount

        self.set_music_volume(self.music_volume - 12)

        return False

    def decrement_score(self, amount = 1):
        """
        Decrements score by <amount>.
        """
        self.score -= amount

        self.set_music_volume(self.music_volume + 12)

        return False

    def set_flag(self, flag, value = True):
        """
        Sets <flag> to <value>, or True if not specified.
        """
        self.flags[flag] = value
        return False

    def get_flag(self, flag):
        """
        Gets the value of <flag>. Returns False if not set.
        """
        return self.flags.get(flag, False)

    def unset_flag(self, flag):
        """
        Unsets <flag>.
        Returns the flag's value or None if flag was not set.
        """
        return self.flags.pop(flag, None)

    def load_music (self, filename):
        """
        Load music file <filename> from "audio/" and set it up for playback.
        Fail silently if sdlmixer couldn't be imported.
        """
        if self.audio_enabled:
            try:
                self.music = Mix_LoadMUS(("audio/" + filename).encode("utf-8"))
            except:
                # TODO: Should be logged, not printed
                print("ERROR: Music file " + filename + " could not be loaded.")
        return False

    def play_music (self, volume = -1):
        """
        Play previously set music file at specified volume, defaults to current
        music_volume.
        Fails silently if file hasn't been loaded.
        """
        if volume < 0:
            volume = self.music_volume
        if self.music is not None:
            Mix_VolumeMusic(volume)
            Mix_PlayMusic(self.music, -1)
        return False

    def stop_music (self, seconds = 0):
        """
        Stop playing music completely. Fades out if seconds > 0.
        """
        if self.music is not None:
            if seconds > 0:
                Mix_HaltMusic()
            else:
                Mix_FadeOutMusic(seconds * 1000)
        return False

    def set_music_volume (self, volume):
        """
        Sets music volume to <volume> or fails silently.
        """
        if self.music is not None:
            try:
                # clamp volume to sane values
                volume = sorted((0, volume, 128))[1]
                Mix_VolumeMusic(volume)
                self.music_volume = volume
            except:
                # volume must be something weird, ignore
                pass
        return False

    def shut_up(self, channel = -1, seconds = 0):
        """
        Fades out <channel> (defaults to all) in <seconds> (defaults to 0).
        """
        if self.audio_enabled:
            if seconds > 0:
                Mix_FadeOutChannel(channel, int(seconds * 1000))
            else:
                Mix_HaltChannel(channel)
        return False

    def quit(self, message = ""):
        """
        Quits the game.
        If a <message> is provided, displays it and waits for keypress first.
        """
        if len(message) > 0:
            self.say("[ " + message + " ]")
            self.pause()
        self.say(-1)
        return False
