#!/usr/bin/python3
# -*- coding: utf-8 -*-
from game import MyGame

class GameUIBase:
    def __init__(self, gs):
        self.game = MyGame(gs)
        self.placeholders = ()


    def __enter__(self):
        return self


    def __exit__(self, exc_type, exc_val, exc_tb):
        """do some cleaning (perhaps)."""
        pass


    def update(self):
        """Update UI with current state."""
        raise NotImplementedError


    def _replace_placeholders(self, text):
        for ph in self.placeholders:
            text = text.replace(ph[0], ph[1])
        return text
