#!/usr/bin/python3
# -*- coding: utf-8 -*-

from helper import *
from time import sleep
from gameui_base import GameUIBase
import readline

class GameUI(GameUIBase):
    def __init__(self, gs):
        super().__init__(gs)

        # Switch to alternate buffer.
        print("\033[?1049h\033[H")

        # Set up autocompletion with a custom completer.
        readline.set_completer(self.__completer)
        readline.parse_and_bind("tab: complete")

        self.placeholders = (
            ('<<completion_key>>', 'tab'),
        )


    def __completer(self, text, state):
        if len(text) == 0:
            return [None]
        text = text.lower()
        objects = [w for w in ("use", "on", "with", "self", "inventory") if w.lower().startswith(text)]
        objects += [obj.name for obj in self.game.gs.inventory if obj.name.lower().startswith(text)]
        objects += [obj.name for obj in self.game.gs.location.objects if obj.name.lower().startswith(text)]
        objects.append(None)
        return objects[state]


    def mainloop(self):
        self.update()
        self.game.gs.clear_story()

        self.game.first_location()

        with self as ui:
            while True:
                res = ui.update()

                if res == -1:
                    return 0
                ui.game.gs.clear_story()

                command = ui.prompt()
                ui.game.parse_command(command)


    def update(self):
        """Update UI with current state."""
        if self.game.gs.location_changed:
            locstr = "You are in {}.".format(self.game.gs.location.name)
            text = ("╭ " + locstr + " ╮\n")
            text += ("╘" + ((len(locstr) + 2) * "═") + "╛")
            print(text)
        for item in self.game.gs.story:
            if isinstance(item, str):
                item = self._replace_placeholders(item)
                print(wrap(item) + "\n")
            elif item == -1:
                return -1
            elif item == 0:
                anykey()
            else:
                sleep(item)


    def prompt(self):
        """Prompt user for a command and return it."""
        command = input('\033[94m> ')
        print('\033[0m', end='')
        return command


    def __exit__(self, exc_type, exc_val, exc_tb):
        # Switch back to primary screen.
        print("\033[0m\033[?1049l")
