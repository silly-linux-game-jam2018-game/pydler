#!/usr/bin/python3
# -*- coding: utf-8 -*-

from helper import *
from time import sleep
from gameui_base import GameUIBase
from picotui.screen import Screen
from custom_tui_widgets import *

class GameUI(GameUIBase):
    def __init__(self, gs):
        super().__init__(gs)
        self.loop = None
        self.location = None
        self.location_text = []
        self.story = None
        self.inventory = None
        self.commandline = None
        self.txt = None

        self.placeholders = (
            ('<<completion_key>>', 'down arrow'),
        )

        # Switch to alternate buffer.
        print("\033[?1049h\033[H")

        self.__init_screen()
        self.__build_screen()


    def mainloop(self):
        self.game.first_location()
        self.update()
        self.dialog.change_focus(self.commandline)
        self.dialog.focus_idx = 5 # HACK: change_focus() doesn't set tab index
        self.dialog.loop()


    def update(self):
        """Update UI with current state."""
        if self.game.gs.location_changed:
            self.location_text = ("> " + self.game.gs.location.name.upper() + " <").center(60)
            self.room.t = self.location_text
            self.room.redraw()

        items = list()
        for obj in self.game.gs.inventory:
            items.append(obj.name)
        self.inventory.set_lines(items)

        if self.game.gs.story is not None:
            txt = []
            for line in self.game.gs.story:
                if type(line) is str:
                    line = self._replace_placeholders(line)
                    line = line.strip()
                    if len(line) > 0:
                        for wrapped_line in wrap(line, 61).split("\n"):
                            txt.append(wrapped_line)
                        txt.append("")
                    else:
                        txt.append(line)
                    self.story.set(txt)
                    if len(txt) > self.story.height:
                        self.story.top_line = len(txt) - self.story.height - 1
                        self.story.row = self.story.height - 1
                    self.dialog.redraw()
                elif line == -1:
                    self.dialog.endloop()
                elif line == 0:
                    self.screen.disable_mouse()
                    anykey()
                    self.screen.enable_mouse()
                elif type(line) is int:
                    sleep(line)
        self.__update_completions()


    def __init_screen(self):
        self.screen = Screen()
        self.screen.init_tty()
        self.screen.enable_mouse()
        self.screen.cls()
        self.dialog = MyDialog(0, 0, 80, 24, "ENDUSER")
        self.dialog.finish_on_esc = False


    def __process_command(self, w):
        self.game.gs.clear_story()
        self.story.top_line = 0
        self.story.cur_line = 0
        self.story.row = 0
        self.story.col = 0
        self.story.set_scrollbar_values()
        command = w.get()
        w.set("")
        w.col = 0
        w.update_line()
        self.game.parse_command(command)
        res = self.update()


    def __update_completions(self):
        objects = ["use", "on", "with", "self", "inventory"]
        objects += [obj.name for obj in self.game.gs.inventory]
        objects += [obj.name for obj in self.game.gs.location.objects]
        self.commandline.items = objects


    def __item_clicked(self, w):
        cmd = self.commandline.get()
        if len(cmd) == 0 or cmd.strip() == "use":
            cmd = "use "
        else:
            if cmd[-1] != " ":
                cmd += " "
            if not cmd.endswith(" on ") and not cmd.endswith(" with "):
                cmd += "on "
        cmd += w.get_cur_line()
        self.commandline.set(cmd)
        self.commandline.col = len(cmd)
        self.dialog.change_focus(self.commandline)
        self.dialog.focus_idx = 4


    def __item_missed(self, w):
        self.dialog.change_focus(self.commandline)
        self.dialog.focus_idx = 4


    def __quit(self, w):
        self.game.gs.clear_story()
        self.game.parse_command("quit")
        self.update()


    def __build_screen(self):
        command_label = MyLabel("Command: ", 9, C_B_BLUE)
        self.dialog.add(1,22, command_label)

        self.room = MyLabel("---", 60, C_B_BLUE)
        self.dialog.add(1, 1, self.room)

        self.b = WButton(3, "X")
        self.dialog.add(76, 1, self.b)
        self.b.on("click", lambda w : self.__quit(w))

        self.storyscrollbar = VScrollBar(18, 0, 0)
        self.dialog.add(62, 3, self.storyscrollbar)

        self.story = TextBox(61, 18, [], self.storyscrollbar)
        self.dialog.add(1, 3, self.story)

        self.commandline = MyCommandLine(69, "", self.story)
        self.commandline.on("enter_used", lambda w : self.__process_command(w))
        self.dialog.add(10, 22, self.commandline)

        inventory_label = MyLabel("Inventory", 15, C_B_GREEN)
        self.dialog.add(64, 3, inventory_label)
        self.inventory = MyList(15, 17, [])
        self.inventory.on("clicked", lambda w : self.__item_clicked(w))
        self.inventory.on("misclick", lambda w : self.__item_missed(w))
        self.dialog.add(64, 4, self.inventory)


    def __exit__(self, exc_type, exc_val, exc_tb):
        Screen.goto(0, 50)
        self.screen.disable_mouse()
        self.screen.cursor(True)
        self.screen.deinit_tty()
        # Switch back to primary screen.
        print("\033[0m\033[?1049l")
