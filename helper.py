#some helper functions
from textwrap import TextWrapper
from shutil import get_terminal_size
import re
from os import system
from sys import stdin
from time import sleep
try:
    from termios import tcflush, TCIOFLUSH
except:
    pass

# Utility functions for text formatting

def wrap(text, width = get_terminal_size()[0]):
    """
    Wraps lines of text (list or string) at <width> columns.
    Defaults to current terminal width.
    """
    res = []
    if isinstance(text, str):
        text = text.split("\n")
    for line in text:
        #Replace ansi/vt100 escape sequences with a placeholder character to
        #mitigate their effect on wrapping.
        if "\033" in line:
            stripped, seqs = strip_ansi(line, "¤")
            res.append(TextWrapper(width,
                break_long_words = False,
                replace_whitespace = True).fill(stripped).replace("¤", "{}").replace("§", " ").format(*seqs))
        else:
            res.append(TextWrapper(width,
                break_long_words = False,
                replace_whitespace = True).fill(line).replace("§", " "))

    return "\n".join(res)

red = lambda text : "\033[91m" + text + "\033[0m"
green = lambda text : "\033[92m" + text + "\033[0m"
yellow = lambda text : "\033[93m" + text + "\033[0m"
blue = lambda text : "\033[94m" + text + "\033[0m"
underline = lambda text : "\033[4m" + text + "\033[24m"
blink = lambda text : "\033[5m" + text + "\033[25m"

def strip_ansi(text, placeholder = ""):
    """
    Strips ansi/vt100 espace sequences and replaces them with <placeholder>.
    Returns a tuple with (newtext, [list of sequences]).
    """
    p = re.compile("(\033\[\d*m)")
    seqs = re.findall(p, text)
    return (re.sub(p, placeholder, text), seqs)

def anykey():
    """
    Waits for keypress and returns.
    """
    try:
        # GNU+Linux or an absolutely proprietary Mac
        sleep(0.2) # give the terminal time to flush the buffer
        tcflush(stdin, TCIOFLUSH) # clear input buffer just in case
        system('/bin/bash -c "read -s -r -n 1"')
        tcflush(stdin, TCIOFLUSH) # key might produce more than one character
    except:
        # An OS this game will probably refuse to run on out of spite
        system("pause")
    return
