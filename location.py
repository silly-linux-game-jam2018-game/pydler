#!/usr/bin/python3
# -*- coding: utf-8 -*-

audio_enabled = False
try:
    from sdl2.sdlmixer import *
    import ctypes
    if Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) != 0:
        audio_enabled = False
    else:
        Mix_Volume(-1, MIX_MAX_VOLUME)
        audio_enabled = True
    Mix_CloseAudio()
except:
    MIX_MAX_VOLUME=128

AUDIO_LOOP = -1
AUDIO_ONCE = 0

class Location:
    """Base class for game locations."""
    def __init__ (self, name, desc, hint = ""):
        self.name = name
        self.objects = []
        self.desc = desc
        self.on_enter = None
        self.on_leave = None
        self.sfx = None
        self.channel = None
        if len(hint) > 0:
            self.hint = hint
        else:
            self.hint = "You can't think of anything to do here. Maybe you should move on."

    def run_on_enter(self):
        if self.on_enter is not None:
            self.on_enter()

    def run_on_leave(self):
        if self.on_leave is not None:
            self.on_leave()

    def add_object (self, obj):
        """Takes a single object and adds it to the location."""
        self.objects.append(obj)

    def add_objects (self, objs):
        """
        Takes a list of objects and adds them to the location.
        Useful for initially populating the list.
        """
        self.objects.extend(objs)

    def remove_object (self, obj):
        """Removes an object from the location."""
        self.objects.remove(obj)

    def load_audio (self, filename):
        """
        Load audio file <filename> from "audio/" and set it up for playback.
        Fail silently if sdlmixer couldn't be imported.
        """
        if audio_enabled:
            try:
                self.sfx = Mix_LoadWAV(("audio/" + filename).encode("utf-8"))
            except:
                print("ERROR: Audio file " + filename + " could not be loaded.")
        return False

    def play_audio (self, volume = MIX_MAX_VOLUME, loops = AUDIO_ONCE, fadein = 0):
        """
        Play associated audio file at specified <volume>, defaults to maximum.
        Fails silently if file hasn't been loaded.

        Optionally set <loops> to AUDIO_LOOP to loop forever and <fadein> to
        the amount of seconds to fade in.
        """
        if self.sfx is not None:
            Mix_VolumeChunk(self.sfx, volume)
            if fadein > 0:
                self.channel = Mix_FadeInChannel(-1, self.sfx, loops, fadein * 1000)
            else:
                self.channel = Mix_PlayChannel(-1, self.sfx, loops)
        return False
