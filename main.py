#!/usr/bin/python3
#
#    PYDLER a toddler tier text adventure engine in python.
#    Mutilated for use in Linux Game Jam 2018.
#    Copyright (C) 2018 tumocs, Samsai & tuubi
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import traceback
from gamestate import GameState
from helper import *

audio_enabled = False
try:
    from sdl2.sdlmixer import *
    import ctypes
    if Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) != 0:
        print("SDL Mixer failed to open an audio device. Disabling audio.")
        audio_enabled = False
    else:
        Mix_Volume(-1, MIX_MAX_VOLUME)
        audio_enabled = True
except:
    print("Running without audio.\nRequires pySDL2, SDL2 and SDL2_Mixer.")

def generate_content():
    print("Generating content file...")

    with open("content.py", "w") as content:
        with open("content_initial.py", "r") as initial:
            content.write(initial.read())
        with open("content_apartment.py", "r") as apartment:
            content.write(apartment.read())
        with open("content_outside.py", "r") as outside:
            content.write(outside.read())
        with open("content_store.py", "r") as store:
            content.write(store.read())

    print("Loading complete.")

#MAIN LOOP
def run(argv):
    generate_content()

    gs = GameState(audio = audio_enabled)
    if "--tui" in argv:
        from gameui_tui import GameUI
    else:
        from gameui_cli import GameUI

    with GameUI(gs) as ui:
        ui.mainloop()
    if audio_enabled:
        Mix_CloseAudio()


if __name__ == "__main__":
    try:
        retval = run(sys.argv)
    except:
        retval = 1
        traceback.print_exc()

    sys.exit(retval)
