#!/bin/bash

cd "`dirname "$0"`"
ARCH=`uname -m`

if [ "$ARCH" == "x86_64" ]; then
    export PYSDL2_DLL_PATH=./lib64/
else
    export PYSDL2_DLL_PATH=./lib/
fi

python3 main.py $1
