#! /bin/sh

if [ -x "$(command -v x-terminal-emulator)" ]; then
    TERMCMD=x-terminal-emulator
elif [ -x "$(command -v termite)" ]; then
    TERMCMD=termite
elif [ -x "$(command -v xfce4-terminal)" ]; then
    TERMCMD=xfce4-terminal
elif [ -x "$(command -v gnome-terminal)" ]; then
    TERMCMD=gnome-terminal
elif [ -x "$(command -v konsole)" ]; then
    TERMCMD=konsole
else
    TERMCMD='xterm -bg black -fg white -xrm "XTerm.vt100.backarrowKey: false" -xrm "xterm*faceName:monospace" -xrm "xterm*faceSize:10"'
fi

$TERMCMD -e "./run.sh $1"
